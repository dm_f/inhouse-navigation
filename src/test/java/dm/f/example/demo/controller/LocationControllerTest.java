package dm.f.example.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.MobileStation;
import dm.f.example.demo.model.Report;
import dm.f.example.demo.service.BaseStationService;
import dm.f.example.demo.service.LocationService;
import dm.f.example.demo.service.MobileStationService;
import dm.f.example.demo.service.ReportService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LocationController.class)
public class LocationControllerTest {

    @MockBean
    private MobileStationService mobileStationService;
    @MockBean
    private BaseStationService baseStationService;
    @MockBean
    private LocationService locationService;
    @MockBean
    private ReportService reportService;
    @InjectMocks
    private LocationController controller;
    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();


    @Test
    public void whenPing_thenPong() throws Exception {
        MvcResult result  = mockMvc.perform(get("/ping")).andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("pong");
    }

    @Test
    public void whenGetMobileStationLocationWithExistingId_thenReturnMobileStation() throws Exception {
        Mockito.when(mobileStationService.getById("x")).thenReturn(new MobileStation(5, 5));

        mockMvc.perform(get("/location/x")).andExpect(status().isOk())
                .andExpect(jsonPath("$.lastKnownX").value(5));
    }

    @Test
    public void whenUpdateMobileStationLocationWithCorrectArguments_thenReturnUpdatedLocation() throws Exception {
        Mockito.when(mobileStationService.moveMS("x", 10, 10))
                .thenReturn(new MobileStation(10, 10));

       mockMvc.perform(post("/location/x/10/10")).andExpect(status().isOk())
               .andExpect(jsonPath("$.lastKnownX").value(10));

    }

    @Test
    public void whenAddMS_thenReturnAddedMSList() throws Exception {
        List<MobileStation> list = new ArrayList<>(Collections.nCopies(3, new MobileStation(5,5)));
        Mockito.when(mobileStationService.addManyMS(any(List.class))).thenReturn(list);

        MvcResult result = mockMvc.perform(post("/location").content(mapper.writeValueAsString(list))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString())
                .isEqualTo("[{\"id\":null,\"lastKnownX\":5.0,\"lastKnownY\":5.0},{\"id\":null,\"lastKnownX\":5.0," +
                        "\"lastKnownY\":5.0},{\"id\":null,\"lastKnownX\":5.0,\"lastKnownY\":5.0}]");
    }

    @Test
    public void whenGetBaseStationReportsWithCorrectId_thenReturnReports() throws Exception {
        Mockito.when(reportService.getById("x")).thenReturn(new Report("x1"));

        mockMvc.perform(get("/base/x")).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("x1"));
    }

    @Test
    public void whenAddBaseStations_thenReturnBaseStationsAdded() throws Exception {
        List<BaseStation> list = new ArrayList<>(Collections.nCopies(3, new BaseStation("aa", 10,10,100)));
        Mockito.when(baseStationService.addManyBs(any(List.class))).thenReturn(list);

        MvcResult result = mockMvc.perform(post("/base").content(mapper.writeValueAsString(list))
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString())
                .isEqualTo("[{\"id\":null,\"name\":\"aa\",\"x\":10.0,\"y\":10.0,\"detectionRadiusInMeters\":100.0}," +
                        "{\"id\":null,\"name\":\"aa\",\"x\":10.0,\"y\":10.0,\"detectionRadiusInMeters\":100.0}," +
                        "{\"id\":null,\"name\":\"aa\",\"x\":10.0,\"y\":10.0,\"detectionRadiusInMeters\":100.0}]");

    }
}
