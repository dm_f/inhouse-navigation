package dm.f.example.demo.service;

import dm.f.example.demo.exception.OutOfLimitException;
import dm.f.example.demo.model.MobileStation;
import dm.f.example.demo.repository.MobileStationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class MobileStationServiceTest {

    @Mock
    private MobileStationRepository mobileStationRepository;
    @InjectMocks
    private MobileStationService mobileStationService;

    @Before
    public void setUp() {
        List<MobileStation> baseStationList = new ArrayList<>(Collections.nCopies(3, new MobileStation()));
        Mockito.when(mobileStationRepository.findAll()).thenReturn(baseStationList);
    }

    @Test
    public void whenFindAll_thenMobileStationsListShouldBeFound() {
        List<MobileStation> found = mobileStationService.getAll();
        assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void whenGetMobileStationByIdWithCorrectId_thenMobileStationShouldBeReturned() {
        Mockito.when(mobileStationRepository.findById("id1"))
                .thenReturn(Optional.of(new MobileStation(5,5)));

        MobileStation returned = mobileStationService.getById("id1");
        assertThat(returned.getLastKnownX()).isEqualTo(5);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetMobileStationByIdWithWrongId_threnResourceNotFoundExceptionShouldBeThrown() {
        Mockito.when(mobileStationRepository.findById("id1"))
                .thenReturn(Optional.of(new MobileStation(5,5)));

        MobileStation returned = mobileStationService.getById("id2");
    }


    @Test
    public void whenSizeLimitIsNotReached_thenReturnListOfFullSize() throws OutOfLimitException {
        List<MobileStation> mobileStationList = new ArrayList<>(Collections.nCopies(3, new MobileStation()));
        Mockito.when(mobileStationRepository.saveAll(mobileStationList)).thenReturn(mobileStationList);

        try {
            List<MobileStation> mobileStationList2 = mobileStationService.addManyMS(mobileStationList);
            assertThat(mobileStationList2.size()).isEqualTo(3);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test(expected = OutOfLimitException.class)
    public void whenSizeLimitIsReached_thenThrowOutOfLimitException() throws OutOfLimitException {
        List<MobileStation> temp = new ArrayList<>(Collections.nCopies(100, new MobileStation()));
        Mockito.when(mobileStationRepository.findAll()).thenReturn(temp);

        List<MobileStation> mobileStationList = new ArrayList<>(Collections.nCopies(3, new MobileStation()));
        List<MobileStation> mobileStationList2 = mobileStationService.addManyMS(mobileStationList);
    }
}
