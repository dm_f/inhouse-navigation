package dm.f.example.demo.service;

import dm.f.example.demo.model.Report;
import dm.f.example.demo.repository.EntryRepository;
import dm.f.example.demo.repository.ReportRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReportServiceTest {

    @Mock
    private EntryRepository entryRepository;
    @Mock
    private ReportRepository reportRepository;
    @InjectMocks
    private ReportService reportService;

    @Before
    public void setUp() {
        List<Report> reportList = new ArrayList<>(Collections.nCopies(3, new Report()));
        Mockito.when(reportRepository.findAll()).thenReturn(reportList);
    }

    @Test
    public void whenFindAll_thenReportsListShouldBeFound() {
        List<Report> found = reportService.getAll();
        assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void whenAddReport_thenReportShouldBeReturned() {
        Mockito.when(reportRepository.save(any(Report.class))).thenReturn(new Report("x1"));
        Report report = new Report("x1");

        Report saved = reportService.addReport(report);
        assertThat(saved.getId()).isEqualTo(report.getId());
    }

    @Test
    public void whenGetReportByIdWithCorrectId_thenReportShouldBeReturned() {
        Mockito.when(reportRepository.findById("id1")).thenReturn(Optional.of(new Report("x1")));

        Report returned = reportService.getById("id1");
        assertThat(returned.getId()).isEqualTo("x1");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetReportByIdWithWrongId_thenResourceNotFoundExceptionShouldBeThrown() {
        Mockito.when(reportRepository.findById("id1")).thenReturn(Optional.of(new Report("x1")));

        Report returned = reportService.getById("id2");
    }
}
