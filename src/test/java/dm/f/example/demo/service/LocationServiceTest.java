package dm.f.example.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static java.lang.Float.NaN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class LocationServiceTest {

    @Mock
    private BaseStationService baseStationService;
    @Mock
    private ReportService reportService;
    @InjectMocks
    private LocationService locationService;

    @Test
    public void whenCalledCalculateDistance_shouldReturnCorrectDistances() {
        float distance = locationService.calculateDistance(5, 5, 10, 20, 50);
        float distance2 = locationService.calculateDistance(1000, 1000, 10, 20, 50);

        assertThat(distance).isEqualTo(15.811388f);
        assertThat(distance2).isEqualTo(NaN);

    }
}
