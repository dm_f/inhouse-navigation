package dm.f.example.demo.service;

import dm.f.example.demo.exception.OutOfLimitException;
import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.repository.BaseStationRepository;
import dm.f.example.demo.repository.ReportRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class BaseStationServiceTest {

    @Mock
    private BaseStationRepository baseStationRepository;
    @Mock
    private ReportRepository reportRepository;
    @InjectMocks
    private BaseStationService baseStationService;

    @Before
    public void setUp() {
        List<BaseStation> baseStationList = new ArrayList<>(Collections.nCopies(3, new BaseStation()));
        Mockito.when(baseStationRepository.findAll()).thenReturn(baseStationList);
    }

    @Test
    public void whenFindAll_thenBaseStationsListShouldBeFound() {
        List<BaseStation> found = baseStationService.getAll();
        assertThat(found.size()).isEqualTo(3);
    }

    @Test
    public void whenGetBaseStationByIdWithCorrectId_thenBaseStationShouldBeReturned() {
        Mockito.when(baseStationRepository.findById("id1"))
                .thenReturn(Optional.of(new BaseStation("a", 3f,2f,3f)));

        BaseStation returned = baseStationService.getById("id1");
        assertThat(returned.getName()).isEqualTo("a");
    }

    @Test(expected = ResourceNotFoundException.class)
    public void whenGetBaseStationByIdWithWrongId_threnResourceNotFoundExceptionShouldBeThrown() {
        Mockito.when(baseStationRepository.findById("id1"))
                .thenReturn(Optional.of(new BaseStation("a", 3f,2f,3f)));

        BaseStation returned = baseStationService.getById("id3");
        assertThat(returned.getName()).isEqualTo("a");
    }

    @Test
    public void whenSizeLimitIsNotReached_thenReturnListOfFullSize() throws OutOfLimitException {
        List<BaseStation> baseStationList = new ArrayList<>(Collections.nCopies(3, new BaseStation()));
        Mockito.when(baseStationRepository.saveAll(baseStationList)).thenReturn(baseStationList);

        try {
            List<BaseStation> baseStationList2 = baseStationService.addManyBs(baseStationList);
            assertThat(baseStationList2.size()).isEqualTo(3);
        } catch (Exception e) {
            throw e;
        }
    }

    @Test(expected = OutOfLimitException.class)
    public void whenSizeLimitIsReached_thenThrowOutOfLimitException() throws OutOfLimitException {
        List<BaseStation> temp = new ArrayList<>(Collections.nCopies(100, new BaseStation()));
        Mockito.when(baseStationRepository.findAll()).thenReturn(temp);

        List<BaseStation> baseStationList = new ArrayList<>(Collections.nCopies(3, new BaseStation()));
        List<BaseStation> baseStationList2 = baseStationService.addManyBs(baseStationList);
    }
}
