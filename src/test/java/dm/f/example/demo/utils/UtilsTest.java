package dm.f.example.demo.utils;

import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.MobileStation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class UtilsTest {

    @Test
    public void whenGivenListAndSizeVarAboveLimit_thenReturnCorrectlyTrimmedList() {
        List<BaseStation> baseStationList = Utils.substarctBsList(Collections.nCopies(20, new BaseStation()),
                90);
        List<MobileStation> mobileStationList = Utils.substarctMsList(Collections.nCopies(20, new MobileStation()),
                90);

        assertThat(baseStationList.size()).isEqualTo(10);
        assertThat(mobileStationList.size()).isEqualTo(10);
    }
}
