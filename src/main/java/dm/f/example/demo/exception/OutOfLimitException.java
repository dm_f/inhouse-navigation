package dm.f.example.demo.exception;

public class OutOfLimitException extends Exception {
    public OutOfLimitException (String s){
        super(s);
    }
}
