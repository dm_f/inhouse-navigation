package dm.f.example.demo.exception;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler  extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {  OutOfLimitException.class })
    protected ResponseEntity<Object> handleOutOfLimitException(OutOfLimitException ex, WebRequest request) {
        return handleExceptionInternal(ex, "Limit of 100 stations reached!", new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    // These methods are overridden to display some custom message
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request)  {
        return handleExceptionInternal(ex, "Method not supported", new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED, request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleExceptionInternal(ex, "Incorrect input params", new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}