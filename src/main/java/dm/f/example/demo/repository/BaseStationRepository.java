package dm.f.example.demo.repository;

import dm.f.example.demo.model.BaseStation;
import org.springframework.data.repository.CrudRepository;

public interface BaseStationRepository extends CrudRepository<BaseStation, String> {
}
