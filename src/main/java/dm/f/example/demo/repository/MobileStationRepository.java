package dm.f.example.demo.repository;

import dm.f.example.demo.model.MobileStation;
import org.springframework.data.repository.CrudRepository;

public interface MobileStationRepository extends CrudRepository<MobileStation, String> {
}
