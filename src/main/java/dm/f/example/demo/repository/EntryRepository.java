package dm.f.example.demo.repository;

import dm.f.example.demo.model.Entry;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface EntryRepository extends CrudRepository<Entry, Long> {
}
