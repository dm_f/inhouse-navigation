package dm.f.example.demo.repository;

import dm.f.example.demo.model.Report;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface ReportRepository extends CrudRepository<Report, String> {
}
