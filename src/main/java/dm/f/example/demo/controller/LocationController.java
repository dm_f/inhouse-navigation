package dm.f.example.demo.controller;


import dm.f.example.demo.exception.OutOfLimitException;
import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.MobileStation;
import dm.f.example.demo.model.Report;
import dm.f.example.demo.service.BaseStationService;
import dm.f.example.demo.service.LocationService;
import dm.f.example.demo.service.MobileStationService;
import dm.f.example.demo.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("")
public class LocationController {

    private final MobileStationService mobileStationService;
    private final BaseStationService baseStationService;
    private final LocationService locationService;
    private final ReportService reportService;

    @Autowired
    public LocationController(MobileStationService mobileStationService, BaseStationService baseStationService, LocationService locationService, ReportService reportService) {
        this.mobileStationService = mobileStationService;
        this.baseStationService = baseStationService;
        this.locationService = locationService;
        this.reportService = reportService;
    }

    @RequestMapping("/ping")
    public String pingPublic() {
        return "pong";
    }


    @GetMapping("/location/{uuid}")
    private ResponseEntity getMobileStationLocation(@PathVariable String uuid) {
        MobileStation ms = mobileStationService.getById(uuid);
        return new ResponseEntity<>(ms, HttpStatus.OK);
    }

    @PostMapping(value = "/location/{uuid}/{x}/{y}", produces = "application/json")
    private ResponseEntity updateMobileStationLocation(@PathVariable String uuid, @PathVariable float x,
                                                      @PathVariable float y) {
        MobileStation ms = mobileStationService.moveMS(uuid, x, y);
        locationService.updateLocations(x, y, uuid);
        return new ResponseEntity<>(ms, HttpStatus.OK);
    }

    @PostMapping(value = "/location", consumes = "application/json")
    private ResponseEntity addMS(@RequestBody @Valid List<MobileStation> mobileStationList) throws OutOfLimitException {
        mobileStationList = mobileStationService.addManyMS(mobileStationList);
        locationService.updateManyLocations(mobileStationList);
        return new ResponseEntity<>(mobileStationList, HttpStatus.OK);
    }

    @GetMapping("/base/{uuid}")
    private Report getBaseStationReports(@PathVariable String uuid) {
        return reportService.getById(uuid);
    }

    @PostMapping(value = "/base", consumes = "application/json")
    private ResponseEntity addBaseStations(@RequestBody @Valid List<BaseStation> baseStationList) throws OutOfLimitException {
        baseStationList = baseStationService.addManyBs(baseStationList);
        locationService.updateLocationsForSpecificBaseStations(mobileStationService.getAll(), baseStationList);
        return new ResponseEntity<>(baseStationList, HttpStatus.OK);
    }

}
