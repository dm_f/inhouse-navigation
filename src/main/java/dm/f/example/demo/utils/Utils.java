package dm.f.example.demo.utils;

import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.MobileStation;

import java.util.List;

public class Utils {

    public static List<BaseStation> substarctBsList(List<BaseStation> list, int size) {
        if (list.size() + size >= 100)
            return list.subList(0, (100-size));
        return list;
    }

    public static List<MobileStation> substarctMsList(List<MobileStation> list, int size) {
        if (list.size() + size >= 100)
            return list.subList(0, (100-size));
        return list;
    }
}
