package dm.f.example.demo.model;

import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="base_station")
public class BaseStation {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)", updatable = false, nullable = false)
    private String id;
    @NotNull
    private String name;
    @NotNull
    private float x;
    @NotNull
    private float y;
    @NotNull
    private float detectionRadiusInMeters;

    public BaseStation(){}

    public BaseStation(String name, float x, float y, float detectionRadiusInMeters) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.detectionRadiusInMeters = detectionRadiusInMeters;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getName() { return name; }

    public float getX() { return x; }

    public float getY() { return y; }

    public float getDetectionRadiusInMeters() { return detectionRadiusInMeters; }

}
