package dm.f.example.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "report")
public class Report {
    @Id
    @Column(name = "base_station_id")
    private String baseStationId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Entry> reports;

    public Report() {reports = new ArrayList<>();}

    public Report(String baseStationId) {
        this.baseStationId = baseStationId;
        reports = new ArrayList<>();
    }

    public String getId() { return baseStationId; }

    public void setId(String id) { this.baseStationId = id; }

    public void addEntry(Entry entry) {
        reports.add(entry);
    }

    public List<Entry> getReports() { return reports; }
}
