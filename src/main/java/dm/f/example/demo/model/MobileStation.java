package dm.f.example.demo.model;

import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="mobile_station")
public class MobileStation {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)", updatable = false, nullable = false)
    private String id;
    @NotNull
    private float lastKnownX;
    @NotNull
    private float lastKnownY;

    public MobileStation(){}

    public MobileStation(float lastKnownX, float lastKnownY) {
//        this.id = id;
        setLastKnownX(lastKnownX);
        setLastKnownY(lastKnownY);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public float getLastKnownX() { return lastKnownX; }

    public void setLastKnownX(float lastKnownX) { this.lastKnownX = lastKnownX; }

    public float getLastKnownY() { return lastKnownY; }

    public void setLastKnownY(float lastKnownY) { this.lastKnownY = lastKnownY; }
}
