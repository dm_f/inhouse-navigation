package dm.f.example.demo.model;

import javax.persistence.GenerationType;
import javax.persistence.Entity;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.sql.Timestamp;

@Entity
public class Entry {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String mobileStationId;
    private float distance;
    private Timestamp timestamp;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Report report;

    public Entry(){}

    public Entry(String mobileStationId, float distance, Timestamp timestamp) {
        this.mobileStationId = mobileStationId;
        this.distance = distance;
        this.timestamp = timestamp;
    }

    public String getMobileStationId() {
        return mobileStationId;
    }

    public float getDistance() {
        return distance;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

}
