package dm.f.example.demo.service;

import dm.f.example.demo.model.Entry;
import dm.f.example.demo.model.Report;
import dm.f.example.demo.repository.EntryRepository;
import dm.f.example.demo.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Not implementing the custom interface for we have only one
 * version of this in the assignment
 */

@Service
public class ReportService {

    private final ReportRepository reportRepository;
    private final EntryRepository entryRepository;

    @Autowired
    public ReportService(ReportRepository reportRepository, EntryRepository entryRepository) {
        this.reportRepository = reportRepository;
        this.entryRepository = entryRepository;
    }

    public Report getById(String uuid) throws ResourceNotFoundException {
        return reportRepository.findById(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Mobile station not found for this id :: " + uuid));
    }

    public List<Report> getAll() {
        return (List<Report>) reportRepository.findAll();
    }

    public Report addReport(Report report) {
        return reportRepository.save(report);
    }

    public Report addReportWithEntry(Report report, Entry entry) {
        report = reportRepository.save(report);
        entryRepository.save(entry);
        return report;
    }

}
