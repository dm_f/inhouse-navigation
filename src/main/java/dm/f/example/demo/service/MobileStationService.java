package dm.f.example.demo.service;

import dm.f.example.demo.exception.OutOfLimitException;
import dm.f.example.demo.model.MobileStation;
import dm.f.example.demo.repository.MobileStationRepository;
import dm.f.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Not implementing the custom interface for we have only one
 * version of this in the assignment
 */

@Service
public class MobileStationService {

    private final MobileStationRepository mobileStationRepository;

    @Autowired
    public MobileStationService(MobileStationRepository mobileStationRepository) {
        this.mobileStationRepository = mobileStationRepository;
    }

    public MobileStation getById(String uuid) throws ResourceNotFoundException  {
        return mobileStationRepository.findById(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Mobile station not found for this id :: " + uuid));
    }

    public List<MobileStation> getAll() {
        return (List<MobileStation>) mobileStationRepository.findAll();
    }

    @Transactional
    public MobileStation moveMS(String uuid, float x, float y) throws ResourceNotFoundException {
        MobileStation ms = mobileStationRepository.findById(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Mobile station not found for this id :: " + uuid));
        ms.setLastKnownX(x);
        ms.setLastKnownY(y);
        return ms;
    }

    public void addMS(MobileStation ms) throws OutOfLimitException {
        if (getAll().size() == 100)
            throw new OutOfLimitException("Limit of mobile stations reached");
        mobileStationRepository.save(ms);
    }

    public List<MobileStation> addManyMS(List<MobileStation> list) throws OutOfLimitException {
        int size = getAll().size();
        if (size == 100)
            throw new OutOfLimitException("Limit of mobile stations reached");

        list = Utils.substarctMsList(list, size);
        return (ArrayList<MobileStation>) mobileStationRepository.saveAll(list);
    }

}
