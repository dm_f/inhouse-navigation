package dm.f.example.demo.service;

import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.Entry;
import dm.f.example.demo.model.MobileStation;
import dm.f.example.demo.model.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import java.util.Timer;
//import java.util.TimerTask;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static java.lang.Float.NaN;

/**
 * Let's imitate the work of real base stations and mobile stations in this simplified version.
 * Since we are lacking real transmission and an antenna, which automatically catches the transmission,
 * we have to explicitly "give" the transmission to each "antenna". So, iterating through the list of
 * base stations is unavoidable. Unless the base station (as a separate thread/event listener e.g.) could know
 * that the transmission belongs to it before distance calculating. Here it is vice versa: BS can know
 * that MS is in the range after the calculations. Alas, we have to iterate through the list then.
 */

@Service
public class LocationService {

    private final BaseStationService baseStationService;
    private final ReportService reportService;

    @Autowired
    public LocationService(BaseStationService baseStationService, ReportService reportService) {
        this.baseStationService = baseStationService;
        this.reportService = reportService;
    }

    protected float calculateDistance(float x, float y, float centerX, float centerY, float radius) {
        double u = Math.pow((x - centerX), 2) + Math.pow((y - centerY), 2);
        if (u < Math.pow(radius, 2))
            // Lets suppose that +-7 digits of precision is ok for us now.
            return (float) Math.sqrt(u);
        return NaN;
    }

    public void updateManyLocations(List<MobileStation> list) {
        list.forEach(mobileStation ->
            updateLocations(mobileStation.getLastKnownX(), mobileStation.getLastKnownY(), mobileStation.getId())
        );
    }

    public void updateLocations(float x, float y, String mobileStationUUID) {
        baseStationService.getAll().forEach(bs -> {
            float distance = calculateDistance(x, y, bs.getX(), bs.getY(), bs.getDetectionRadiusInMeters());
            if (distance != NaN) {
                Report report = reportService.getById(bs.getId());
                report.addEntry(new Entry(mobileStationUUID, distance, new Timestamp(new Date().getTime())));
                reportService.addReport(report);
            }
        });
    }

    public void updateLocationsForSpecificBaseStations(List<MobileStation> mobileStationList,
                                                       List<BaseStation> baseStationList) {
        mobileStationList.forEach(ms -> {
            baseStationList.forEach(bs -> {
                float distance = calculateDistance(ms.getLastKnownX(), ms.getLastKnownY(), bs.getX(), bs.getY(),
                        bs.getDetectionRadiusInMeters());
                if (distance != NaN) {
                    Report report = reportService.getById(bs.getId());
                    report.addEntry(new Entry(ms.getId(), distance, new Timestamp(new Date().getTime())));
                    reportService.addReport(report);
                }
            });
        });

    }


//    private Timer timer = new Timer();
//    private int begin = 0;
//    private int timeInterval = 1000;
//
//    public void startMobileStationsSearch() {
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//
//                // This could be also implemented in a scanning scenario, when base stations "scan the area"
//                // each n seconds
//
//            }
//        }, begin, timeInterval);
//    }

}
