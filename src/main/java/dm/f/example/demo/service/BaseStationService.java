package dm.f.example.demo.service;

import dm.f.example.demo.exception.OutOfLimitException;
import dm.f.example.demo.model.BaseStation;
import dm.f.example.demo.model.Report;
import dm.f.example.demo.repository.BaseStationRepository;
import dm.f.example.demo.repository.ReportRepository;
import dm.f.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Not implementing the custom interface for we have only one
 * version of this in the assignment
 */

@Service
public class BaseStationService {

    private final BaseStationRepository baseStationRepository;
    private final ReportRepository reportRepository;

    @Autowired
    public BaseStationService(BaseStationRepository baseStationRepository, ReportRepository reportRepository) {
        this.baseStationRepository = baseStationRepository;
        this.reportRepository = reportRepository;
    }

    public BaseStation getById(String uuid) {
        return baseStationRepository.findById(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Base station not found for this id :: " + uuid));
    }

    public List<BaseStation> getAll() {
        return (List<BaseStation>) baseStationRepository.findAll();
    }

    public List<BaseStation> addManyBs(List<BaseStation> list) throws OutOfLimitException {
        int size = getAll().size();
        if (size == 100)
            throw new OutOfLimitException("Limit of base stations reached");

        list = Utils.substarctBsList(list, size);
        list = (List<BaseStation>) baseStationRepository.saveAll(list);

        // Creating empty reports for each base station
        list.forEach(baseStation -> reportRepository.save(new Report(baseStation.getId())));
        return list;
    }

}
