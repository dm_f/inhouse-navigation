package dm.f.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Lets pretend we have real base stations. In that case we could not do the simple magic of
 * taking their range coordinates in one single thread with one listener, which will compare
 * the coordinates of mobile stations
 */

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
